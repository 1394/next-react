import React from "react";
import Link from "next/link";
import Router from "next/router";

const Home = () => {
    // 编程式跳转
    const gotoB = () => {
        Router.push('/04/lyy');
    }

    return (
        <>
            <div>我是首页</div>
            {/* 标签跳转 */}
            <div><Link href="/04/ljj">去Ljj-A~~page</Link></div>
            <div><Link href="/04/lyy">去Lyy-B~~page</Link></div>
            <div style={{ marginTop: '10px' }}>
                {/* <button onClick={() => Router.push('/04/lyy')}>去Lyy-B~~page</button> */}
                <button onClick={gotoB}>去Lyy-B~~page</button>
            </div>
        </>
    )
}

export default Home;