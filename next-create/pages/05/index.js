import React from "react";
import Link from "next/link";
import router from "next/router";

const Home = () => {
    const gotoXiaojiejie = () => {
        // router.push("/05/xiaojiejie?name=井空");
        router.push({
            pathname: "/05/xiaojiejie",
            query: {
                name: "井空"
            }
        })
    }
    // routerChangeStart 路由发生变化时
    // routerChangeComplete 路由结束变化时
    // beforeHistoryChange 浏览器history触发前
    // routeChangeError 路由跳转发生错误时
    // hashChangeStart 转变成hash路由模式
    // hashChangeComplete 转变成hash路由模式

    router.events.on('routerChangeStart', (...args) => {
        console.log('1.routeChangeStart->路由开始变化,参数为:', ...args);
    })
    router.events.on('routerChangeComplete', (...args) => {
        console.log('2.routerChangeComplete->路由结束变化,参数为:', ...args);
    })
    router.events.on('beforeHistoryChange', (...args) => {
        console.log('3.beforeHistoryChange->在改变浏览器 history之前触发,参数为:', ...args);
    })
    router.events.on('routeChangeError', (...args) => {
        console.log('4.routeChangeError->跳转发生错误,参数为:', ...args);
    })
    router.events.on('hashChangeStart', (...args) => {
        console.log('5.hashChangeStart->hash跳转开始时执行,参数为:', ...args);
    })
    router.events.on('hashChangeComplete', (...args) => {
        console.log('6.hashChangeComplete->hash跳转完成时,参数为:', ...args);
    })

    return (
        <>
            <div>我是首页</div>
            <div>
                <Link href="/05/xiaojiejie?name=结衣">
                    选择结衣
                </Link>
                <br />
                <Link href="/05/xiaojiejie?name=井空">
                    选择井空
                </Link>
                <br />
                <button onClick={gotoXiaojiejie}>选择井空</button>
                <br />
                <Link href={{ pathname: "/05/xiaojiejie", query: { name: "结衣" } }}>
                    选择结衣
                </Link>
                <br />
            </div>
        </>
    )
}

export default Home;