import { withRouter } from "next/router";
import Link from "next/link";
import axios from 'axios'

const xiaojiejie = ({ router, list }) => {
    return (
        <>
            <div>{router.query.name}，来为我服务了。</div>
            <div>{list}</div>
            <Link href="/05/">返回首页</Link>
        </>
    )
}

xiaojiejie.getInitialProps = async () => {
    const promise = new Promise((resolve) => {
        axios('https://www.fastmock.site/mock/6ff52d018330d0650bed38718fde63b1/redux/getList').then((res) => {
            console.log('远程数据结果：', res);
            resolve(res.data.data);
        });
    })
    return await promise;
}

export default withRouter(xiaojiejie);