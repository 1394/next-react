// import Head from "next/head";
import MyHeader from "../../components/myheader";
import { Button } from 'antd';
import { SmileTwoTone } from '@ant-design/icons';

const Header = () => {
    return (
        <>
            {/* <Head>
                <title>胖哥是个小阳人！</title>
                <meta charSet="utf-8" />
            </Head> */}
            <MyHeader></MyHeader>
            <div>JSPang.com</div>
            <div><Button type="primary">我是一个按钮</Button></div>
            <SmileTwoTone />
        </>
    )
}

export default Header;