import React, { useState, useEffect } from 'react';
// import moment from "moment";
import dynamic from "next/dynamic";

const One = dynamic(import('../../components/one'));

const Times = () => {
    const [nowTime, setNowTime] = useState(null);

    useEffect(() => {
        setNowTime(Date.now());
    }, []);

    // 改变时间格式    
    // const changeTime = () => {
    //     setNowTime(moment(Date.now()).format('YYYY MMMM Do, h:mm:ss')); // 注意使用defalut
    // }

    const changeTime = async () => { // 把方法变成异步模式
        const moment = await import('moment'); // 等待moment加载完成
        setNowTime(moment.default(Date.now()).format('YYYY MMMM Do, h:mm:ss')); // 注意使用defalut
    }

    return (
        <>
            <div>显示时间为：{nowTime}</div>
            <One />
            <div><button onClick={changeTime}>改变时间格式</button></div>
        </>
    )
}

export default Times;