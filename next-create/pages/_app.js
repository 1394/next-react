// 添加全局样式表
import "../static/css/test.css"

// 如果pages下没有_app.js文件，需新创建`_app.js`文件中必须有此默认的导出（export）函数
export default function MyApp({ Component, pageProps }) {
    return <Component {...pageProps} />
}